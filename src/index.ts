import axios from 'axios'
import cheerio from 'cheerio'
import fs from 'fs'

const listUrl: string = 'https://doujindesu.xxx/komik-list/'

const countPage = async (url: string = listUrl): Promise<string[]> => {
  const { data } = await axios.get(url)
  const $ = cheerio.load(data)
  const pagination = $('.pagination').find('span')
  const text = Object(Array.from(Object(pagination[0]).children)[0]).data
  const page = text.replace(/Page 1 of ([0-9]+)/g, '$1')

  let pages: string[] = [listUrl]
  for (let i = 0; i < page; i++) {
    if (i === 0) continue
    pages.push(`${listUrl}page/${i + 1}`)
  }
  return pages
}
const getDoujin = async (pageUrl: string): Promise<any[]> => {
  const { data } = await axios.get(pageUrl)
  const $ = cheerio.load(data)
  const list = Array.from($('.relat').find('article'))
  const formated: object[] = list.map((data) => {
    const { href, title } = Object(cheerio.load(data)('a'))[0].attribs
    const thumbnail = Object(cheerio.load(data)('img'))[0].attribs.src
    return { title, url: href, thumbnail }
  })
  return formated
}

const getChapterList = async (url: string, t: string) => {
  try {
    const { data } = await axios.get(url)
    const $ = cheerio.load(data)
    const li = Array.from($(`.lstepsiode.listeps`).find('li'))
    const list = li.map((data) => {
      const listData = cheerio.load(data)
      const chapterDetail = Object(listData('.lchx').find('a'))[0]
      const url = chapterDetail.attribs.href
      const title =
        Object(chapterDetail.children[0]).data === undefined
          ? t
          : Object(chapterDetail.children[0]).data
      return { url, title }
    })
    return list.reverse()
  } catch (error) {
    console.log(error)
    return []
  }
}

const getChapterPages = async (url: string) => {
  const { data } = await axios.get(url)
  const $ = cheerio.load(data)
  const readerArea = Array.from($('.reader-area').find('img'))
  const result = readerArea.map((data) => {
    return Object(data).attribs.src
  })

  return result
}

const getAll = async () => {
  const pages = await countPage()

  for (const page of pages) {
    const numberOfPages = `${pages.indexOf(page) + 1}`
    const pageResult = await getDoujin(page)
    for (const pages of pageResult) {
      let doujinChapters = []
      const chapters = await getChapterList(pages.url, pages.title)
      for (const chapter of chapters) {
        const images = await getChapterPages(chapter.url)
        doujinChapters.push({
          chapter: chapters.indexOf(chapter) + 1,
          ...chapter,
          images,
        })
      }

      const hasil = {
        ...pages,
        chapters: doujinChapters,
      }

      console.log(numberOfPages, hasil)
      const pageNumber = `${pageResult.indexOf(pages) + 1}`
      fs.writeFileSync(
        `./dump/${Array.from(Array(6 - pageNumber.length), () => 0).join(
          ''
        )}${pageNumber}-${encodeURIComponent(pages.title)}.json`,
        JSON.stringify(hasil, null, 2)
      )
    }
  }
}

getAll()
